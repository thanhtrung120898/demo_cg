#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
    int new_x;
    int new_y;
    
    new_x = xc + x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);

    
    new_x = xc - x;
    new_y = yc + y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    

    
    new_x = xc - x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
    

    
    new_x = xc + x;
    new_y = yc - y;
    SDL_RenderDrawPoint(ren, new_x, new_y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
   
        double p,m,n;
        int x,y;
        m=a*a;
        n=b*b;
        x=0;
        y=b;
        
        p=2*(n/m)-(2*b)+1;
        
         // Area 1
        while((n/m)*x<=y)
        {
            if(p<0)
            {
                p=p+2*((n/m)*(2*x+3));
                Draw4Points(xc, yc, x, y, ren);

            }
            else{
                p= p- 4*y + 2*(n/m)*(2*x+3);
                y--;
                Draw4Points(xc, yc, x, y, ren);

            }
            x++;
        }
     // Area 2
        y=0;
        x=a;
        p=2*(m/n)-2*a+1;
        while((m/n)*y<=x)
        {
            if(p<0)
            {
                p=p +2*(m/n)*(2*y+3);
                Draw4Points(xc, yc, x, y, ren);

            }
            else
            {
                p=p- 4*x + 2*(m/n)*(2*y+3);
                x--;
                Draw4Points(xc, yc, x, y, ren);

            }
            y=y+1;
        }
    
  

   
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    int x, y, n, m, p;
    x = 0;
    y = b;
    m = a*a;
    n = b*b;
    Draw4Points(xc, yc, x, y, ren);
     // Area 1
    p = n - m*b +m/4;
    while((n/m)*x<=y)
    {
        if(p<0)
        {
            p += n*(2*x + 3);
        }
        else
        {
            y--;
            p += n*(2*x +3) + m*(2- 2*y);
            
        }
        x++;

        Draw4Points(xc, yc, x, y, ren);
    }
    
      // Area 2
    
    p = n*(x +1/2)*(x +1/2) + m*(y-1)*(y-1) - m*n;
    while(y>0)
    {
        
        if(p >=0)
        {
            p += m*(3-2*y);
        }
        else
        {
            x++;
            p += n*(2*x +2) +m*(3- 2*y);
        }
        y--;

        Draw4Points(xc, yc, x, y, ren) ;

    }
   

  
    
    


}
